<#import "/spring.ftl" as spring />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="/static/styles/main.css" type="text/css" />
    <title>
        Web studio "Prestige-Art" Изготовление сайтов на заказ любой сложности.
        Дизайн сайтов, полиграфии. Мы не делаем просто сайты, мы делаем шедевры!
    </title>
    <script type="text/javascript" src="/static/js/app.js"></script>
</head>

<body>

    <h3>Сообщения, поступившие из формы обратной связи</h3>
    <ul>
    <#list messages as message>
        <li>${message?html}</li>
    </#list>
    </ul>

    <h3>Заказы</h3>

    <h4>Сайты:</h4>
    <ul>
    <#if orderSites?size = 0>
        <li>Нет заказов</li>
    <#else>
        <#list orderSites as order>
        <li>${order?html}</li>
        </#list>
    </#if>
    </ul>

    <h4>Полиграфия:</h4>
    <ul>
    <#if orderPrintings?size = 0>
        <li>Нет заказов</li>
    <#else>
        <#list orderPrintings as order>
        <li>${order?html}</li>
        </#list>
    </#if>
    </ul>

    <h4>Корпоративный стиль:</h4>
    <ul>
    <#if orderStyles?size = 0>
        <li>Нет заказов</li>
    <#else>
        <#list orderStyles as order>
        <li>${order?html}</li>
        </#list>
    </#if>
    </ul>

</body>
</html>
