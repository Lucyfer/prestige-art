<#import "../layout.ftl" as layout>
<#import "/spring.ftl" as spring />

<@layout.base>
    <div class='desc'>
        <p class='descr'>По всем интересующим Вас вопросам можно обратиться по:</p>

        <table>
            <tr>
                <td>Адрес:</td>
                <td>г. Барнаул, ул. Малотобольская 20, офис 3</td>
            </tr>
            <tr>
                <td>Время работы:</td>
                <td>9.00–21.00</td>
            </tr>
            <tr>
                <td><img src='/static/img/im_phn.png' alt='phone' /> Телефон:</td>
                <td>+7 913-026-11-61 </td>
            </tr>
            <tr>
                <td><img src='/static/img/im_mal.png' alt='mail' />E-mail:</td>
                <td><a href="mailto:prestige-art@ya.ru">prestige-art@ya.ru</a> - общие вопросы </td>
            </tr>
            <tr>
                <td><img src='/static/img/im_icq.png' alt='icq' />ICQ:</td>
                <td>312-067-482</td>
            </tr>
        </table>

        <form action='/feedback/save' method='post'>
            <@spring.bind "feedback" />
            <h3>ФОРМА СВЯЗИ</h3>

            <blockquote class='descr'>
                Вы можете отправить нам сообщение и мы ответим вам
                в самые кратчайшие сроки.
            </blockquote>

            <#if flashError??>
                <div class="flash error">${flashError}</div>
            </#if>

            <table>
                <tr>
                    <td><label for="name">ФИО *</label></td>
                    <td>
                        <@spring.formInput "feedback.name" />
                        <@spring.showErrors "feedback.name", "error" />
                    </td>
                </tr>
                <tr>
                    <td><label for="org">Организация</label></td>
                    <td>
                        <@spring.formInput "feedback.company" />
                        <@spring.showErrors "feedback.company", "error" />
                    </td>
                </tr>
                <tr>
                    <td><label for="email">E-mail *</label></td>
                    <td>
                        <@spring.formInput "feedback.email" />
                        <@spring.showErrors "feedback.email", "error" />
                    </td>
                </tr>
                <tr>
                    <td><label for="phone">Контактный телефон</label></td>
                    <td>
                        <@spring.formInput "feedback.phone" />
                        <@spring.showErrors "feedback.phone", "error" />
                    </td>
                </tr>
                <tr>
                    <td><label for="msg">Ваше сообщение *</label></td>
                    <td>
                        <@spring.formTextarea "feedback.message", "rows='2' cols='40'" />
                        <@spring.showErrors "feedback.message", "error" />
                    </td>
                </tr>
            </table>
            <p class='descr'>
                <label for="antispam">Введите код защиты от спама*:</label>
                <img src='/captcha/image.png' alt='...' />
                <input id="antispam" class='descr' type='text' name='antispam' size='5' maxlength='5' />
                <input class='descr' type='submit' value='Отправить' />
                <#if spring.status.errors.hasGlobalErrors()>
                    <span class="error">
                    <#list spring.status.errors.getGlobalErrors() as error>
                        <@spring.message error.code />
                    </#list>
                    </span>
                </#if>
            </p>
            <p class="attention">* обязательные поля</p>
        </form>
    </div>
</@layout.base>
