<#import "../layout.ftl" as layout>

<@layout.base>

<div class='desc'>
    <p>	Сайт – один из лучших инструментов современного бизнеса.
        Активное использование интернет &mdash; технологий позволит Вам вывести свой бизнес на качественно новый уровень,
        поскольку Интернет на сегодняшний день является одним из наиболее прогрессивных, перспективных и интересных
        областей, работа в которой позволит Вашей компании освоить новый рынок, наладить более тесное сотрудничество
        со своими партнерами, расширить горизонты своей деятельности. <br />
        &nbsp;&nbsp;&nbsp; Современный интернет-сайт должен соответствовать высоким требованиям. Интересный дизайн,
        широкие функциональные возможности, качественное информационное наполнение, &mdash; это одни из основных требований,
        предъявляемых к интернет - ресурсам нового поколения.
        Команда профессионалов компании «Prestige-Art» способна решить все задачи, касающиеся Вашего сайта.
        Выгода размещения информации о Вашей компании в Интернете и её реклама очевидна. Потенциальный заказчик или
        клиент имеет возможность узнать о Вашей фирме, её предложениях и подробностях, гораздо больше, чем вы сами смогли
        рассказать о ней по телефону. Речь идет об огромном объеме информации, которую фирма сочтет необходимым
        предоставить клиентом. Это могут быть статьи, фотографии, рисунки, даже фильмы.
    </p>
</div>

<div class='mid1'>
    <b class='red_text'>Изготовление сайтов:</b><br />
    <a href='/static/popup/site.html' onclick='open_p(this.href,this.text);return false;'>- Сайт-визитка</a> <br />
    <a href='/static/popup/portal.html' onclick='open_p(this.href,this.text);return false;'>- Корпоративный портал</a> <br />
    <a href='/static/popup/shop.html' onclick='open_p(this.href,this.text);return false;'>- Интернет магазин</a>
</div>

<div class='mid2'>
    <b class='red_text'>Дизайн полиграфии:</b><br />
    <a href='/static/popup/card.html' onclick='open_p(this.href,this.text);return false;'>- Дизайн визиток</a>  <br />
    <a href='/static/popup/poster.html' onclick='open_p(this.href,this.text);return false;'>- Дизайн плакатов</a> <br />
    <a href='/static/popup/booklet.html' onclick='open_p(this.href,this.text);return false;'>- Дизайн буклетов</a>
</div>

<div class='mid3'>
    <b class='red_text'>Дизайн фирменного стиля:</b><br />
    <a href='/static/popup/corporation.html' onclick='open_p(this.href,this.text);return false;'>- Корпоративный дизайн</a> <br />
    <a href='/static/popup/trademark.html' onclick='open_p(this.href,this.text);return false;'>- Дизайн товарного знака</a> <br />
    <a href='/static/popup/logo.html' onclick='open_p(this.href,this.text);return false;'>- Дизайн календарей</a>
</div>

<div class='last_pro'>
    <p class='header'>Последние проекты</p>

    <table class='center'>
        <tr>
            <td>
                <a href='http://ADDRESS1'> <img src='/static/img/site/last1.jpg' alt='' />FineMebel - мебельный магазин</a>
            </td>
            <td>
                <a href='http://ADDRESS2'> <img src='/static/img/site/last2.jpg' alt='' />Покрывала - магазин белья</a>
            </td>
            <td>
                <a href='http://ADDRESS3'> <img src='/static/img/site/last3.jpg' alt='' />Косметика Онлайн - магазин</a>
            </td>
        </tr>
    </table>

    <p class='right'>
        <a href='/all' class='all_sites'>Все работы &gt;&gt;</a>
    </p>
</div>

</@layout.base>
