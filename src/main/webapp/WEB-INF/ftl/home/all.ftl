<#import "../layout.ftl" as layout>

<@layout.base>
<div class='desc'>
    <p class='descr'>
        Все работы
    </p>
</div>

<div class='last_pro'>
    <p class='header'>Последние проекты</p>

    <table class='center'>
        <tr>
            <td>
                <a href='http://ADDRESS1'><img src='/static/img/site/last1.jpg' alt='' />FineMebel - мебельный магазин</a>
            </td>
            <td>
                <a href='http://ADDRESS2'> <img src='/static/img/site/last2.jpg' alt='' />Покрывала - магазин белья</a>
            </td>
            <td>
                <a href='http://ADDRESS3'> <img src='/static/img/site/last3.jpg' alt='' />Косметика Онлайн - магазин</a>
            </td>
        </tr>
    </table>

    <p class='right'>
        <a href='/all' class='all_sites'>Все работы &gt;&gt;</a>
    </p>
</div>
</@layout.base>
