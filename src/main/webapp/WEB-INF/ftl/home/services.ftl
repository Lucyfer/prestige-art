<#import "../layout.ftl" as layout>

<@layout.base>
    <div class='desc'>
        <div class='descr'>
            <p class="descr">- Создание Веб сайтов:</p>
            <ul>
                <li>Сайт визитка</li>
                <li>Корпоративный сайт</li>
                <li>Интернет магазин</li>
                <li>Дизайн сайтов</li>
                <li>Редизайн существующих сайтов</li>
                <li>Продвижение сайтов</li>
            </ul>
        </div>

        <div class='descr'>
            <p class="descr">- Дизайн полиграфии:</p>
            <ul>
                <li>Дизайн каталогов</li>
                <li>Дизайн визиток  </li>
                <li>Дизайн брошюр  	</li>
                <li>Дизайн буклетов	</li>
                <li>Дизайн плакатов, афиш </li>
                <li>Дизайн календарей </li>
            </ul>
        </div>

        <div class='descr'>
            <p class="descr">- Дизайн фирменного стиля, торговой марки:</p>
            <ul>
                <li>Корпоративный дизайн </li>
                <li>Дизайн товарного знака, логотипа, фирменного знака </li>
            </ul>
        </div>

        <div class="center">
            <a class='descr' href='/order'>Перейти к форме заказа</a>
        </div>
    </div>
</@layout.base>
