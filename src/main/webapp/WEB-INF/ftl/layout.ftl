<#macro base>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="/static/styles/main.css" type="text/css" />
    <title>
        Web studio "Prestige-Art" Изготовление сайтов на заказ любой сложности.
        Дизайн сайтов, полиграфии. Мы не делаем просто сайты, мы делаем шедевры!
    </title>
    <script type="text/javascript" src="/static/js/app.js"></script>
</head>

<body>
    <img alt='' src='/static/img/top.jpg'    class='itop' />
    <img alt='' src='/static/img/middle.jpg' class='imid' />
    <img alt='' src='/static/img/bottom.jpg' class='ibot' />

    <a class='home' href="#" title='сделать стартовой (работает только для IE)'
       onclick="this.style.behavior='url(#default#homepage)';return false;">
        <img src='/static/img/home.png' alt='2' />
    </a>

    <a class='mail' href="mailto:prestige-art@yandex.ru" title='написать e-mail'>
        <img src='/static/img/mail.png' alt='1' />
    </a>

    <a href='/about' target='_self'>
        <img src='/static/img/menu/about.jpg' class='menu_a' alt='О нас' />
    </a>

    <a href='/portfolio' target='_self'>
        <img src='/static/img/menu/port.jpg' class='menu_p' alt='Портфолио' />
    </a>

    <a href='/services' target='_self'>
        <img src='/static/img/menu/service.jpg' class='menu_s' alt='Услуги' />
    </a>

    <a href='/order/' target='_self' >
        <img src='/static/img/menu/demand.jpg' class='menu_d' alt='Сделать заказ' />
    </a>

    <a href='/contacts' target='_self' >
        <img src='/static/img/menu/contact.jpg' class='menu_c' alt='Контакты' />
    </a>

    <div class='welcome'>Добро пожаловать на наш сайт!</div>

    <#nested>

    <div class='menu2'>
        <a href="/" target="_self">Главная</a>             |
        <a href='/about' target='_self'>О нас</a>          |
        <a href='/portfolio' target='_self'>портфолио</a>  |
        <a href='/services' target='_self'>услуги</a>      |
        <a href='/order/' target='_self'>сделать заказ</a> |
        <a href='/contacts' target='_self'>контакты</a>
    </div>
</body>
</html>

</#macro>
