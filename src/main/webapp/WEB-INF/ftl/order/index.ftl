<#import "/spring.ftl" as spring />

<!DOCTYPE HTML>
<html>
<head>
    <script language='javascript' src="/static/js/app.js" type="text/javascript"></script>
    <link rel="stylesheet" href="/static/styles/main.css" type="text/css" />
    <style type="text/css">
        A IMG {
            border: 0;
        }

        P, TD, UL {
            font-family: sans-serif;
            font-size: 12px;
        }
    </style>
</head>
<body class="orange">
<h2>Что Вам нужно сделать?</h2>

<#if flashError??>
<div class="flash error">${flashError}</div>
</#if>

<p>
    <a href='#' onclick='return change("id1")'>
        <img src='/static/img/add.png' alt='+'/>
        Создание сайта
    </a>
</p>

<div class="hidden" id='id1'>
<form action='/order/save/site' method='post'>
<@spring.bind "orderSite" />
    <div>
        <p>Контактная информация</p>
        <table>
            <tr>
                <td>Контактное лицо *</td>
                <td>
                    <@spring.formInput "orderSite.name" />
                    <@spring.showErrors "orderSite.name", "error" />
                </td>
            </tr>
            <tr>
                <td>Телефон *</td>
                <td>
                    <@spring.formInput "orderSite.phone" />
                    <@spring.showErrors "orderSite.phone", "error" />
                </td>
            </tr>
            <tr>
                <td>Е-майл *</td>
                <td>
                    <@spring.formInput "orderSite.email" />
                    <@spring.showErrors "orderSite.email", "error" />
                </td>
            </tr>
        </table>
    </div>
    <p>
        <label>
            Официальное название предприятия<br/>
            <@spring.formInput "orderSite.companyTitle", "size='50'" />
            <@spring.showErrors "orderSite.companyTitle", "error" />
        </label>
    </p>
    <p>
        <label>
            Ваши конкуренты (сайты, названия компаний)<br/>
            <@spring.formInput "orderSite.rivals", "size='50'" />
            <@spring.showErrors "orderSite.rivals", "error" />
        </label>
    </p>
    <p>
        <label>
            Целевая аудитория (пол, возраст, образование, уровень дохода,
            особенности стиля жизни и т.п.)<br/>
            <@spring.formInput "orderSite.targetAudience", "size='50'" />
            <@spring.showErrors "orderSite.targetAudience", "error" />
        </label>
    </p>
    <div>
        <p>Тип сайта:</p>
        <ul class="markerless">
            <li>
                <label>
                    <input type='radio' name='orderSite.siteType' value='1'
                       checked="<#if orderSite.siteType!0 = 1>checked<#else>none</#if>" />
                    <@spring.message "orderSite.siteType.cutaway"/>
                </label>
            </li>
            <li>
                <label>
                    <input type='radio' name='orderSite.siteType' value='2'
                       checked="<#if orderSite.siteType!0 = 2>checked<#else>none</#if>" />
                    <@spring.message "orderSite.siteType.corporate"/>
                </label>
            </li>
            <li>
                <label>
                    <input type='radio' name='orderSite.siteType' value='3'
                       checked="<#if orderSite.siteType!0 = 3>checked<#else>none</#if>" />
                    <@spring.message "orderSite.siteType.shop"/>
                </label>
            </li>
        </ul>
    </div>

    <p>
        <label>
            Бюджет заказа
            <@spring.formInput "orderSite.budget", "size='5'" /> руб.
            <@spring.showErrors "orderSite.budget", "error" />
        </label>
    </p>

    <p>
        <label>
            Рекомендации по концепции разработки сайта: <br/>
            <@spring.formTextarea "orderSite.wishList", "rows='5' cols='40'" />
            <@spring.showErrors "orderSite.wishList", "error" />
        </label>
    </p>

    <hr/>
    <div>
        <label>
            Введите код защиты от спама*:
            <img src='/captcha/image.png' alt='...'/>
            <input type='text' name='antispam' size='5' maxlength='5'/>
        </label>
        <input type='submit' value='Отправить'/>
        <#if spring.status.errors.hasGlobalErrors()>
            <span class="error">
                <#list spring.status.errors.getGlobalErrors() as error>
                    <@spring.message error.code />
                </#list>
            </span>
        </#if>
    </div>
    <span class="attention">* поля обязательные для заполнения</span>
</form>
</div>

<p>
    <a href='#' onclick='return change("id2")'>
        <img src='/static/img/add.png' alt='+'/>
        Заказ дизайна полиграфии:
    </a>
</p>

<div id='id2' class="hidden">
    <form action='/order/save/printing' method='post'>
    <@spring.bind "orderPrinting" />
        <div>
            <p>Контактная информация</p>
            <table>
                <tr>
                    <td>Контактное лицо *</td>
                    <td>
                    <@spring.formInput "orderPrinting.name" />
                    <@spring.showErrors "orderPrinting.name", "error" />
                    </td>
                </tr>
                <tr>
                    <td>Телефон *</td>
                    <td>
                    <@spring.formInput "orderPrinting.phone" />
                    <@spring.showErrors "orderPrinting.phone", "error" />
                    </td>
                </tr>
                <tr>
                    <td>Е-майл *</td>
                    <td>
                    <@spring.formInput "orderPrinting.email" />
                    <@spring.showErrors "orderPrinting.email", "error" />
                    </td>
                </tr>
            </table>
        </div>
        <p>
            <label>
                Официальное название предприятия<br/>
                <@spring.formInput "orderPrinting.companyTitle", "size='50'" />
                <@spring.showErrors "orderPrinting.companyTitle", "error" />
            </label>
        </p>

        <p>
            Какие элементы фирменного стиля Вы хотели бы использовать? <br/>
            <@spring.formInput "orderPrinting.styleElements", "size='50'" />
            <@spring.showErrors "orderPrinting.styleElements", "error" />
        </p>

        <hr/>
        <p>
            <label>
                Введите код защиты от спама*:
                <img src='/captcha/image.png' alt='captcha'/>
                <input type='text' name='antispam' size='5' maxlength='5'/>
            </label>
            <input type='submit' name='subm' value='Отправить'/>
            <#if spring.status.errors.hasGlobalErrors()>
                <span class="error">
                    <#list spring.status.errors.getGlobalErrors() as error>
                        <@spring.message error.code />
                    </#list>
                </span>
            </#if>
        </p>
        <span class="attention">* поля обязательные для заполнения</span>
    </form>
</div>

<p>
    <a href='#' onclick='return change("id3")'>
        <img src='/static/img/add.png' alt='+'/>
        Фирменный стиль
    </a>
</p>

<div id="id3" class="hidden">
    <form id='id3' action='/order/save/style' method='post'>
    <@spring.bind "orderStyle" />
        <div>
            <p>Контактная информация</p>
            <table>
                <tr>
                    <td>Контактное лицо *</td>
                    <td>
                    <@spring.formInput "orderStyle.name" />
                    <@spring.showErrors "orderStyle.name", "error" />
                    </td>
                </tr>
                <tr>
                    <td>Телефон *</td>
                    <td>
                    <@spring.formInput "orderStyle.phone" />
                    <@spring.showErrors "orderStyle.phone", "error" />
                    </td>
                </tr>
                <tr>
                    <td>Е-майл *</td>
                    <td>
                    <@spring.formInput "orderStyle.email" />
                    <@spring.showErrors "orderStyle.email", "error" />
                    </td>
                </tr>
            </table>
        </div>
        <p>
            <label>
                Официальное название предприятия<br/>
                <@spring.formInput "orderStyle.companyTitle", "size='50'" />
                <@spring.showErrors "orderStyle.companyTitle", "error" />
            </label>
        </p>
        <div>
            <p>Цветовое решение:</p>
            <@spring.formInput "orderStyle.colors", "size='50'" />
            <@spring.showErrors "orderStyle.colors", "error" />
        </div>

        <hr/>
        <p>
            <label>
                Введите код защиты от спама*:
                <img src='/captcha/image.png' alt='captcha'/>
                <input type='text' name='antispam' size='5' maxlength='5'/>
            </label>
            <input type='submit' name='subm' value='Отправить'/>
            <#if spring.status.errors.hasGlobalErrors()>
                <span class="error">
                    <#list spring.status.errors.getGlobalErrors() as error>
                            <@spring.message error.code />
                        </#list>
                </span>
            </#if>
        </p>
        <span class="attention">* поля обязательные для заполнения</span>
    </form>
</div>

<#if type??>
<script language='javascript'>
    change('id${type}');
</script>
</#if>

</body>
</html>
