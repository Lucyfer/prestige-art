<#import "../layout.ftl" as layout>

<@layout.base>
    <div class="desc">
        <h2>Спасибо за заказ!</h2>
        <h3>С Вами скоро свяжутся для обсуждения деталей</h3>
    </div>
</@layout.base>
