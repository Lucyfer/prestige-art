/**
 * Open Popup Window
 * @param uri URI
 * @param title Window's title
 */
function open_p(uri, title) {
    var paramstr = 'toolbar=no,location=no,left=50,top=50,directories=no,status=yes,' +
                   'menubar=no,scrollbars=yes,resizable=yes,width=400,height=500';
    var popupWin = window.open(uri, title, paramstr);
    popupWin.focus();
}

/**
 * Change visibility
 * @param id Element's id
 * @returns {boolean} Always returns false
 */
function change(id) {
    var e = document.getElementById(id);
    if (e != null && e != undefined) {
        var invisible = e.style.display == 'none' || e.style.display == '';
        e.style.display = invisible ? 'block' : 'none';
    }
    return false;
}
function turn_on(id, action) {
    var e = document.getElementById(id);
    e.disabled = !action;
}
function turn(id) {
    var e = document.getElementById(id);
    e.disabled = !e.disabled;
}
