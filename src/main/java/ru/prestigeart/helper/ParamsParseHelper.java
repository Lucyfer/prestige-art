package ru.prestigeart.helper;

public class ParamsParseHelper {

    public static Integer parseInt(String s) {
        Integer result;
        try {
            result = Integer.parseInt(s.trim());
        }
        catch (Exception e) {
            result = null;
        }
        return result;
    }
}
