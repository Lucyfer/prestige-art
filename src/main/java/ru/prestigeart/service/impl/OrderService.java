package ru.prestigeart.service.impl;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.prestigeart.domain.dao.IOrderPrintingDAO;
import ru.prestigeart.domain.dao.IOrderSiteDAO;
import ru.prestigeart.domain.dao.IOrderStyleDAO;
import ru.prestigeart.domain.model.OrderPrinting;
import ru.prestigeart.domain.model.OrderSite;
import ru.prestigeart.domain.model.OrderStyle;
import ru.prestigeart.service.IOrderService;

import java.util.List;

@Repository
public class OrderService implements IOrderService {

    @Autowired
    private IOrderPrintingDAO orderPrintingDAO;

    @Autowired
    private IOrderSiteDAO orderSiteDAO;

    @Autowired
    private IOrderStyleDAO orderStyleDAO;

    @Override
    public void saveOrderSite(OrderSite order) throws HibernateException {
        orderSiteDAO.saveOrUpdate(order);
    }

    @Override
    public void saveOrderPrinting(OrderPrinting order) throws HibernateException {
        orderPrintingDAO.saveOrUpdate(order);
    }

    @Override
    public void saveOrderStyle(OrderStyle order) throws HibernateException {
        orderStyleDAO.saveOrUpdate(order);
    }

    @Override
    public List<OrderSite> findAllSiteOrders() {
        return orderSiteDAO.findAll();
    }

    @Override
    public List<OrderPrinting> findAllPrintOrders() {
        return orderPrintingDAO.findAll();
    }

    @Override
    public List<OrderStyle> findAllStyleOrders() {
        return orderStyleDAO.findAll();
    }
}
