package ru.prestigeart.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import ru.prestigeart.service.ICaptchaService;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

@Service("myCaptchaService")
@Scope("singleton")
public class CaptchaService implements ICaptchaService {
    final static String alphabet = "123456789qwertyuiopasdfghjkzxcvbnmQWERTYUIPASDFGHJKLZXCVBNM";
    final static Random random = new Random();
    final static Font font = new Font("Monospaced", Font.BOLD, 18);
    final static int captchaLength = 4;
    final static int imageWidth = 45;
    final static int imageHeight = 20;

    @Autowired
    HttpSession session;

    @Override
    public void initialize() {
        session.setAttribute("captcha", generateRandomString());
    }

    @Override
    public void validate(String userInput, BindingResult result) {
        String sessionCaptcha = (String)session.getAttribute("captcha");
        if (!userInput.equalsIgnoreCase(sessionCaptcha)) {
            result.addError(new ObjectError(
                    "captcha", new String[] {"captcha.validation.incorrect"},
                    null, "Проверочный код введен неверно"));
        }
    }

    @Override
    public String generateRandomString() {
        final int strLen = alphabet.length();
        char[] chars = new char[captchaLength];

        for (int i = 0; i < chars.length; ++i) {
            chars[i] = alphabet.charAt(random.nextInt(strLen));
        }
        return String.valueOf(chars);
    }

    @Override
    public void generateImageWithString(String text, OutputStream stream) {
        BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, imageWidth, imageHeight);
        if (!text.isEmpty()) {
            g.setColor(Color.blue);
            g.setFont(font);
            g.drawString(text, 0, 15);
            g.drawLine(0, random.nextInt() % imageHeight, imageWidth, random.nextInt() % imageHeight);
        }
        try {
            ImageIO.write(image, "png", stream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
