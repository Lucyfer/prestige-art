package ru.prestigeart.service.impl;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.prestigeart.domain.dao.IFeedbackDAO;
import ru.prestigeart.domain.model.Feedback;
import ru.prestigeart.service.IFeedbackService;

import java.util.List;

@Repository
public class FeedbackService implements IFeedbackService {

    @Autowired
    private IFeedbackDAO feedbackDAO;

    @Override
    public List<Feedback> findAll() throws HibernateException {
        return feedbackDAO.findAll();
    }

    @Override
    public void save(Feedback feedback) throws HibernateException {
        feedbackDAO.saveOrUpdate(feedback);
    }
}
