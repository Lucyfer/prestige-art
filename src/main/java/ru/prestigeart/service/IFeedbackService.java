package ru.prestigeart.service;

import ru.prestigeart.domain.model.Feedback;

import java.util.List;

public interface IFeedbackService {
    List<Feedback> findAll();
    void save(Feedback feedback);
}
