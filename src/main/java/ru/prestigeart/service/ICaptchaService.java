package ru.prestigeart.service;

import org.springframework.validation.BindingResult;

import java.io.OutputStream;

public interface ICaptchaService {
    void initialize();
    String generateRandomString();
    void generateImageWithString(String text, OutputStream stream);
    void validate(String userInput, BindingResult result);
}
