package ru.prestigeart.service;

import ru.prestigeart.domain.model.OrderPrinting;
import ru.prestigeart.domain.model.OrderSite;
import ru.prestigeart.domain.model.OrderStyle;

import java.util.List;

public interface IOrderService {
    void saveOrderPrinting(OrderPrinting order);
    void saveOrderSite(OrderSite order);
    void saveOrderStyle(OrderStyle order);

    List<OrderSite> findAllSiteOrders();
    List<OrderPrinting> findAllPrintOrders();
    List<OrderStyle> findAllStyleOrders();
}
