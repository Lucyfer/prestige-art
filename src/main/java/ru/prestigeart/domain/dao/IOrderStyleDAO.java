package ru.prestigeart.domain.dao;

import ru.prestigeart.domain.model.OrderStyle;

public interface IOrderStyleDAO extends IGenericDAO<OrderStyle, Long> {
}
