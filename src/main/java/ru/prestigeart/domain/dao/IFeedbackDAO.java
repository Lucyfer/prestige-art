package ru.prestigeart.domain.dao;

import ru.prestigeart.domain.model.Feedback;

public interface IFeedbackDAO extends IGenericDAO<Feedback, Long> {
}
