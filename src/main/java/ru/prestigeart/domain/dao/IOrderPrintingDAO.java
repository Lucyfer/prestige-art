package ru.prestigeart.domain.dao;

import ru.prestigeart.domain.model.OrderPrinting;

public interface IOrderPrintingDAO extends IGenericDAO<OrderPrinting, Long> {
}
