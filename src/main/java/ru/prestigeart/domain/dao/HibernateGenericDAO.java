package ru.prestigeart.domain.dao;

import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

@SuppressWarnings("unchecked")
public abstract class HibernateGenericDAO<T, ID extends Serializable>
        implements IGenericDAO<T, ID> {

    @Autowired
    protected SessionFactory sessionFactory;

    protected final Class<T> entityClass;

    public HibernateGenericDAO() {
        this.entityClass = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    @Override
    public T findById(ID id) throws HibernateException {
        Transaction t = sessionFactory.getCurrentSession().beginTransaction();
        try {
            T entity = (T)sessionFactory.getCurrentSession().load(entityClass, id);
            t.commit();
            return entity;
        }
        catch (HibernateException e) {
            t.rollback();
            throw e;
        }
    }

    @Override
    public List<T> findAll() {
        Transaction t = sessionFactory.getCurrentSession().beginTransaction();
        try {
            List<T> lst = sessionFactory.getCurrentSession()
                                        .createCriteria(entityClass).list();
            t.commit();
            return lst;
        }
        catch (HibernateException e) {
            t.rollback();
            throw e;
        }
    }

    @Override
    public void saveOrUpdate(T entity) throws HibernateException {
        Transaction t = sessionFactory.getCurrentSession().beginTransaction();
        try {
            sessionFactory.getCurrentSession().saveOrUpdate(entity);
            t.commit();
        }
        catch (HibernateException e) {
            t.rollback();
            throw e;
        }
    }
}
