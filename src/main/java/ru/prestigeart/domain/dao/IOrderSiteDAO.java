package ru.prestigeart.domain.dao;

import ru.prestigeart.domain.model.OrderSite;

public interface IOrderSiteDAO extends IGenericDAO<OrderSite, Long> {
}
