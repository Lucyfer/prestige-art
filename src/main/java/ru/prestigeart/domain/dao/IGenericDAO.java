package ru.prestigeart.domain.dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDAO<T, ID extends Serializable> {
    T findById(ID id);
    List<T> findAll();
    void saveOrUpdate(T entity);
}
