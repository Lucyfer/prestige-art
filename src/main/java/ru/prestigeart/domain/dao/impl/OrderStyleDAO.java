package ru.prestigeart.domain.dao.impl;

import org.springframework.stereotype.Repository;
import ru.prestigeart.domain.dao.HibernateGenericDAO;
import ru.prestigeart.domain.dao.IOrderStyleDAO;
import ru.prestigeart.domain.model.OrderStyle;

@Repository
public class OrderStyleDAO extends HibernateGenericDAO<OrderStyle, Long>
        implements IOrderStyleDAO {
}
