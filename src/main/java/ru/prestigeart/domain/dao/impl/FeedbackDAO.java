package ru.prestigeart.domain.dao.impl;

import org.springframework.stereotype.Repository;
import ru.prestigeart.domain.dao.HibernateGenericDAO;
import ru.prestigeart.domain.dao.IFeedbackDAO;
import ru.prestigeart.domain.model.Feedback;

@Repository
public class FeedbackDAO extends HibernateGenericDAO<Feedback, Long>
        implements IFeedbackDAO {
}
