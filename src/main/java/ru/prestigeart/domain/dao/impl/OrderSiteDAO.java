package ru.prestigeart.domain.dao.impl;

import org.springframework.stereotype.Repository;
import ru.prestigeart.domain.dao.HibernateGenericDAO;
import ru.prestigeart.domain.dao.IOrderSiteDAO;
import ru.prestigeart.domain.model.OrderSite;

@Repository
public class OrderSiteDAO extends HibernateGenericDAO<OrderSite, Long>
        implements IOrderSiteDAO {
}
