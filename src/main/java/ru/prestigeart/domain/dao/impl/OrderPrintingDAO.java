package ru.prestigeart.domain.dao.impl;

import org.springframework.stereotype.Repository;
import ru.prestigeart.domain.dao.HibernateGenericDAO;
import ru.prestigeart.domain.dao.IOrderPrintingDAO;
import ru.prestigeart.domain.model.OrderPrinting;

@Repository
public class OrderPrintingDAO extends HibernateGenericDAO<OrderPrinting, Long>
        implements IOrderPrintingDAO {
}
