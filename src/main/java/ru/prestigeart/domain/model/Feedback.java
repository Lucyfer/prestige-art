package ru.prestigeart.domain.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Feedback {

    @Id
    @GeneratedValue
    @Column
    private Long id;

    @Column(nullable = false)
    @NotEmpty
    private String name;

    @Column
    private String company;

    @Column(nullable = false)
    @NotEmpty
    private String email;

    @Column
    private String phone;

    @Column(columnDefinition = "TEXT", nullable = false)
    @NotEmpty
    private String message;

    @Column(updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date messageTimestamp) {
        this.created = messageTimestamp;
    }

    @Override
    public String toString() {
        return "[" + created + "] #" + id + " " + name + " (" +
                email + ") {" + message + "}";
    }
}
