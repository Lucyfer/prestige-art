package ru.prestigeart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.prestigeart.domain.model.Feedback;
import ru.prestigeart.service.impl.CaptchaService;
import ru.prestigeart.service.impl.FeedbackService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private FeedbackService feedbackService;

    @RequestMapping(value = {"/", ""})
    public String index() {
        return "redirect:/contacts";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView saveFeedback(
            @Valid @ModelAttribute("feedback")Feedback feedback,
            BindingResult errors,
            @RequestParam(value = "antispam", defaultValue = "")String antispam) {
        captchaService.validate(antispam, errors);
        if (errors.hasErrors()) {
            captchaService.initialize();
            Map<String, Object> model = new HashMap<String, Object>();
            model.put("feedback", feedback);
            return new ModelAndView("home/contacts", model);
        } else {
            try {
                feedbackService.save(feedback);
                return new ModelAndView("redirect:/feedback/thanks");
            }
            catch (Exception e) {
                captchaService.initialize();
                Map<String, Object> model = new HashMap<String, Object>();
                model.put("feedback", feedback);
                model.put("flashError", e.getLocalizedMessage());
                e.printStackTrace();
                return new ModelAndView("home/contacts", model);
            }
        }
    }

    @RequestMapping("/thanks")
    public String thanks() {
        return "feedback/thanks";
    }
}
