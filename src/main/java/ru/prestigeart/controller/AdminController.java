package ru.prestigeart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.prestigeart.domain.model.Feedback;
import ru.prestigeart.domain.model.OrderPrinting;
import ru.prestigeart.domain.model.OrderSite;
import ru.prestigeart.domain.model.OrderStyle;
import ru.prestigeart.service.impl.FeedbackService;
import ru.prestigeart.service.impl.OrderService;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private FeedbackService feedbackService;

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = {"/", ""})
    public String index(Model model) {
        List<Feedback> msgLst = feedbackService.findAll();
        model.addAttribute("messages", msgLst);

        List<OrderSite> siteLst = orderService.findAllSiteOrders();
        model.addAttribute("orderSites", siteLst);

        List<OrderPrinting> printLst = orderService.findAllPrintOrders();
        model.addAttribute("orderPrintings", printLst);

        List<OrderStyle> styleLst = orderService.findAllStyleOrders();
        model.addAttribute("orderStyles", styleLst);

        return "admin/index";
    }
}
