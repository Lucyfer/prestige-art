package ru.prestigeart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import ru.prestigeart.service.ICaptchaService;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@Controller
@RequestMapping("/captcha")
@SessionAttributes("captcha")
public class CaptchaController {

    @Autowired
    private ICaptchaService captchaService;

    @RequestMapping("/image.png")
    public void getCaptcha(
            HttpServletResponse response,
            @ModelAttribute("captcha") String captcha) {
        response.addHeader("Cache-control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
        response.addHeader("Content-type", "image/png");
        try {
            captchaService.generateImageWithString(captcha, response.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @ModelAttribute("captcha")
    public String populateCaptcha() {
        return captchaService.generateRandomString();
    }
}
