package ru.prestigeart.controller;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.prestigeart.helper.ParamsParseHelper;
import ru.prestigeart.domain.model.OrderPrinting;
import ru.prestigeart.domain.model.OrderSite;
import ru.prestigeart.domain.model.OrderStyle;
import ru.prestigeart.service.ICaptchaService;
import ru.prestigeart.service.IOrderService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Controller
@RequestMapping("/order")
@SessionAttributes("captcha")
public class OrderController {

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ICaptchaService captchaService;

    @Autowired
    private IOrderService orderService;

    /**
     * Форма заказа
     * @param requestType сайт=1/полиграфия=2/стиль=3
     * @return model + view
     */
    @RequestMapping(value = {"/", ""})
    public ModelAndView index(
            @RequestParam(value = "type", defaultValue = "") String requestType,
            HttpSession httpSession) {
        Integer type = ParamsParseHelper.parseInt(requestType);
        Map<String, Object> modelArgs = new HashMap<String, Object>();
        OrderSite orderSite = new OrderSite();
        OrderPrinting orderPrinting = new OrderPrinting();
        OrderStyle orderStyle = new OrderStyle();

        captchaService.initialize();
        modelArgs.put("type", type);
        modelArgs.put("orderSite", orderSite);
        modelArgs.put("orderPrinting", orderPrinting);
        modelArgs.put("orderStyle", orderStyle);

        return new ModelAndView("order/index", modelArgs);
    }

    /**
     * Заказ сайта
     * @return view
     */
    @RequestMapping(value = "/save/site", method = RequestMethod.POST)
    public ModelAndView saveSite(
            @Valid @ModelAttribute("orderSite") OrderSite orderSite,
            BindingResult errors, Locale locale,
            @RequestParam(value = "antispam", defaultValue = "")String antispam) {
        Map<String, Object> modelArgs = new HashMap<String, Object>();

        modelArgs.put("type", 1);
        captchaService.validate(antispam, errors);

        if (errors.hasErrors()) {
            captchaService.initialize();
            modelArgs.put("orderSite", orderSite);
            modelArgs.put("flashError", messageSource.getMessage("errors.found", null, locale));
        } else {
            try {
                orderService.saveOrderSite(orderSite);
                return new ModelAndView("redirect:/order/thanks");
            }
            catch (HibernateException e) {
                modelArgs.put("flashError", e.getLocalizedMessage());
                modelArgs.put("orderSite", orderSite);
            }
        }
        return new ModelAndView("/order/index", modelArgs);
    }

    /**
     * Заказ полиграфии
     * @return view
     */
    @RequestMapping(value = "/save/printing", method = RequestMethod.POST)
    public ModelAndView savePrinting(
            @Valid @ModelAttribute("orderPrinting") OrderPrinting orderPrinting,
            BindingResult errors, Locale locale,
            @RequestParam(value = "antispam", defaultValue = "")String antispam) {
        Map<String, Object> modelArgs = new HashMap<String, Object>();

        modelArgs.put("type", 2);
        captchaService.validate(antispam, errors);

        if (errors.hasErrors()) {
            captchaService.initialize();
            modelArgs.put("orderPrinting", orderPrinting);
            modelArgs.put("flashError", messageSource.getMessage("errors.found", null, locale));
        } else {
            try {
                orderService.saveOrderPrinting(orderPrinting);
                return new ModelAndView("redirect:/order/thanks");
            }
            catch (HibernateException e) {
                modelArgs.put("flashError", e.getLocalizedMessage());
                modelArgs.put("orderPrinting", orderPrinting);
            }
        }
        return new ModelAndView("/order/index", modelArgs);
    }

    /**
     * Заказ фирменного стиля
     * @return view
     */
    @RequestMapping(value = "/save/style", method = RequestMethod.POST)
    public ModelAndView saveStyle(
            @Valid @ModelAttribute("orderStyle") OrderStyle orderStyle,
            BindingResult errors, Locale locale,
            @RequestParam(value = "antispam", defaultValue = "")String antispam) {
        Map<String, Object> modelArgs = new HashMap<String, Object>();

        modelArgs.put("type", 3);
        captchaService.validate(antispam, errors);

        if (errors.hasErrors()) {
            captchaService.initialize();
            modelArgs.put("orderStyle", orderStyle);
            modelArgs.put("flashError", messageSource.getMessage("errors.found", null, locale));
        } else {
            try {
                orderService.saveOrderStyle(orderStyle);
                return new ModelAndView("redirect:/order/thanks");
            }
            catch (HibernateException e) {
                modelArgs.put("flashError", e.getLocalizedMessage());
                modelArgs.put("orderStyle", orderStyle);
            }
        }
        return new ModelAndView("/order/index", modelArgs);
    }

    @RequestMapping(value = "/thanks")
    public String thanks() {
        return "order/thanks";
    }
}
