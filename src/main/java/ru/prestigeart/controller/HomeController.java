package ru.prestigeart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.prestigeart.domain.model.Feedback;
import ru.prestigeart.service.impl.CaptchaService;

@Controller
public class HomeController {

    @Autowired
    private CaptchaService captchaService;

    @RequestMapping("/")
    public String index() {
        return "home/index";
    }

    @RequestMapping("/about")
    public String about() {
        return "home/about";
    }

    @RequestMapping("/portfolio")
    public String portfolio() {
        return "home/portfolio";
    }

    @RequestMapping("/services")
    public String services() {
        return "home/services";
    }

    @RequestMapping("/contacts")
    public String contacts(Model model) {
        captchaService.initialize();
        model.addAttribute("feedback", new Feedback());
        return "home/contacts";
    }

    @RequestMapping("/all")
    public String all() {
        return "home/all";
    }
}
