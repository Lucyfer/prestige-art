package ru.prestigeart.helper;

import junit.framework.TestCase;

public class ParamsParseHelperTest extends TestCase {

    public void testParseInt() throws Exception {
        assertEquals(null, ParamsParseHelper.parseInt(""));
        assertEquals(null, ParamsParseHelper.parseInt("ab"));
        assertEquals(null, ParamsParseHelper.parseInt("@#$"));
        assertEquals((Integer)0, ParamsParseHelper.parseInt("0"));
        assertEquals((Integer)0, ParamsParseHelper.parseInt("000"));
        assertEquals((Integer)125, ParamsParseHelper.parseInt("0125"));
        assertEquals((Integer)1256, ParamsParseHelper.parseInt("  01256  "));
    }
}
