package ru.prestigeart.service.impl;

import junit.framework.TestCase;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import javax.servlet.http.HttpSession;

import java.io.OutputStream;

import static org.mockito.Mockito.*;

public class CaptchaServiceTest extends TestCase {

    CaptchaService captchaService;

    @Override
    protected void setUp() throws Exception {
        captchaService = new CaptchaService();
    }

    @Override
    protected void tearDown() throws Exception {
        captchaService = null;
    }

    public void testInitialize() throws Exception {
        captchaService.session = mock(HttpSession.class);

        captchaService.initialize();
        verify(captchaService.session, times(1)).setAttribute(eq("captcha"), anyString());
    }

    public void testGenerateRandomString() throws Exception {
        final String captcha = captchaService.generateRandomString();

        assertEquals(CaptchaService.captchaLength, captcha.length());
        for (int i=0; i<captcha.length(); i++) {
            String s = String.valueOf(captcha.charAt(i));
            assertTrue(CaptchaService.alphabet.contains(s));
        }
    }

    public void testValidate() throws Exception {
        captchaService.session = mock(HttpSession.class);
        when(captchaService.session.getAttribute("captcha")).thenReturn("someCaptcha");

        BindingResult r1 = mock(BindingResult.class);
        captchaService.validate("invalid", r1);
        verify(r1).addError(any(ObjectError.class));

        BindingResult r2 = mock(BindingResult.class);
        captchaService.validate("someCaptcha", r2);
        verify(r2, never()).addError(any(ObjectError.class));
    }

    public void testGenerateImageWithString() throws Exception {
        OutputStream stream = mock(OutputStream.class);

        // mustn't throw exceptions
        captchaService.generateImageWithString("", stream);
        captchaService.generateImageWithString("q2NU", stream);
        captchaService.generateImageWithString("q2NU", null);
    }
}
